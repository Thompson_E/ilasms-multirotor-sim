import time
import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm

from src.acinit import T18
from src.wind import WindForce
from src.functions import get_translate_consts, convert_wind

s = time.perf_counter()

NAME = "DFW4"

ac = T18(dt=0.01)

wind = WindForce()
xy = np.loadtxt(f'input/{NAME}-xy.csv', delimiter=',')
x = xy[:, 0]
y = xy[:, 1]
z = [32]*len(x)

_, geo_ref, offset, consts = get_translate_consts(f'input/{NAME}-latlon.csv')

# weather = convert_wind(NAME, geo_ref, offset, consts)

t = np.array(list(zip(x, y, z)))

ac.initilise_trajectory(t)

v = np.zeros(3)
prev_pos = np.zeros(3)

wv = np.random.uniform(-10, 0, 3)
wv[2] = 0

winddata = [wv]

# plt.plot(np.array(winddata)[:, 0])
# plt.plot(np.array(winddata)[:, 1])

prev_pos_wind = np.zeros(3)

pos_hist = [[0, 0, 0]]

for i, pos in tqdm(enumerate(ac.trajectory), leave=False):
    if i == 300000:
        break

    if i*ac.dt % 1 == 0:
        wv = np.clip(wv+np.random.uniform(-1, 1, 3), -12, 2)
        wv[2] = 0

        plt.clf()
        plt.plot(np.array(pos_hist)[:, 0], np.array(pos_hist)[:, 1])
        plt.pause(0.05)

        winddata.append(np.array(wv))

        # plt.clf()
        # plt.plot(np.array(winddata)[:, 0])
        # plt.plot(np.array(winddata)[:, 1])

        # plt.pause(0.05)

    drag = wind.get_drag(v, wv)

    state = ac.step(pos, drag[:-1])
    # print(pos)
    pos_ = state[:3]

    pos_hist.append(pos_)

    v = state[3:6]

    prev_pos = pos_


ac.log.done_logging()

print(time.perf_counter() - s, i)

plt.plot(ac.log.target[:, 0], ac.log.target[:, 1], label='Prescribed traj')
plt.plot(ac.log.x, ac.log.y, label='Actual traj')

plt.show()
