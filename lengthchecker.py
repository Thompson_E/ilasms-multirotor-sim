import numpy as np
from math import sqrt
import geopy.distance
from math import cos
import matplotlib.pyplot as plt


points = np.loadtxt(fname='input/DFW4-xy.csv', delimiter=',')

print(points)

prev = points[0]

dist = 0

for i in range(1, len(points)):
    dist += sqrt((points[i][0]-prev[0])**2 + (points[i][1]-prev[1])**2)/1000
    prev = points[i]

print(dist)

plt.plot(points[:, 1], points[:, 0])
plt.show()

coords = np.loadtxt(fname='input/DFW4-latlon.csv', delimiter=',')
cdist = sum(geopy.distance.distance(
    coords[i], coords[i-1]).km for i in range(1, len(coords)))
print(cdist)

mse = (cdist*1000) - (dist*1000)

print(mse)
