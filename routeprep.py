import itertools
import json
import os
import time

import geopy.distance
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pyproj
from math import cos

from src.functions import getXYfromLL, get_translate_consts
from WeatherGen.src.weatherHandler import request_weather

geodesic = pyproj.Geod(ellps='WGS84')

OUT_PATH = "out/"
IN_PATH = "routes/"

alts = [30, 35, 40]
times = ["2021-04-20T05:30:00+00:00",
         "2021-04-20T05:45:00+00:00", "2021-04-20T06:00:00+00:00"]


def main(_name: str):
    _out_path = f'{OUT_PATH}/{_name}-{hash(time.time())}'

    os.makedirs(name=_out_path, exist_ok=True)

    converted_points, origional_path = prep_rout_const(
        f'{IN_PATH}/{_name}.csv')

    np.savetxt(f'{_out_path}/{_name}-xy.csv', converted_points, delimiter=',')
    np.savetxt(f'{_out_path}/{_name}-latlon.csv',
               origional_path, delimiter=',')

    df, j_file = prep_weather(f'{IN_PATH}/{_name}.csv', origional_path)

    df.to_csv(f'{_out_path}/{_name}-weather.csv', index=False)
    json.dump(j_file, open(f'{_out_path}/{_name}-weather.json', 'w'))


def prep_rout_const(_name):
    geo_track, geo_ref, offset, consts = get_translate_consts(_name)

    return np.array([getXYfromLL(pos, geo_ref, offset, consts) for pos in geo_track]), geo_track


def prep_route(_name, max_dist=25):
    route_path = _name

    coordinates = np.loadtxt(route_path, delimiter=',')
    print(f'Loaded route with {coordinates.shape[0]} points')

    converted = np.array([[0, 0]], dtype=(float, float))

    for i in range(1, coordinates.shape[0]):
        az12, _, dist = geodesic.inv(
            coordinates[i-1, 1], coordinates[i-1, 0], coordinates[i, 1], coordinates[i, 0])

        x = converted[-1, 0] + dist * np.sin(np.deg2rad(az12))
        y = converted[-1, 1] + dist * np.cos(np.deg2rad(az12))

        x = np.linspace(converted[-1, 0], x,
                        int(np.ceil(dist/max_dist)), dtype=float)
        y = np.linspace(converted[-1, 1], y,
                        int(np.ceil(dist/max_dist)), dtype=float)

        co = np.array(list(zip(x, y)))

        converted = np.append(converted, co, axis=0) if i != 1 else co

    print(f'Converted route to {converted.shape[0]} points')
    return converted, coordinates


def prep_weather(_name, coordinates):
    min_lat, max_lat = coordinates[:, 0].min(), coordinates[:, 0].max()
    min_lon, max_lon = coordinates[:, 1].min(), coordinates[:, 1].max()

    az12, az21, a = geodesic.inv(
        min_lon, min_lat, max_lon, max_lat)

    new_min = geopy.distance.distance(meters=100).destination(
        point=(min_lat, min_lon), bearing=(az12-180) % 360)

    new_min = (new_min.latitude, new_min.longitude)

    new_max = geopy.distance.distance(meters=100).destination(
        point=(max_lat, max_lon), bearing=(az21-180) % 360)
    new_max = (new_max.latitude, new_max.longitude)

    lat_dist = geopy.distance.distance(
        (new_min[0], new_min[1]), (new_max[0], new_min[1])).meters

    lon_dist = geopy.distance.distance(
        (new_min[0], new_min[1]), (new_min[0], new_max[1])).meters

    lat_interval = int(np.ceil(lat_dist / 150))
    lon_interval = int(np.ceil(lon_dist / 150))

    print(
        f'Using the following intervals for lattice generation: ({lat_interval},{lon_interval})')

    interval_lats = np.linspace(new_min[0], new_max[0], lat_interval)
    interval_lons = np.linspace(new_min[1], new_max[1], lon_interval)

    points = np.array(np.meshgrid(
        interval_lats, interval_lons)).T.reshape(-1, 2)
    print(f'There are {points.shape[0]} points in the lattice')

    qlats, qlons, qalts, qtimes = [], [], [], []

    print(
        f'Building up lattice with {len(alts)} altitudes and {len(times)} times')

    _, origin, rotation, consts = get_translate_consts(_name)
    for point in points:
        for alt, time in itertools.product(alts, times):
            qlats.append(float(point[0]))
            qlons.append(float(point[1]))
            qalts.append(float(alt))
            qtimes.append(time)

    print(f'There are {len(qlats)} queried points in total')

    print('Leaving program to query the weather API, please wait...')
    data = request_weather(
        qlats, qlons, qalts, qtimes)

    df = pd.DataFrame([[float(item['lat']), float(item['lon']), float(item['alt']), str(item['timestamp']),
                      float(item['wind_dir']), float(item['wind_speed'])] for _, item in data.items()], columns=['lat', 'lon', 'alt', 'timestamp', 'wind_dir', 'wind_speed'])

    print(df.head)

    return df, data


if __name__ == "__main__":
    main("LinearRoute")
