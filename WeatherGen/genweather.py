import itertools
import matplotlib.pyplot as plt
import numpy as np
import pyproj
import geopy.distance
from src.weatherHandler import request_weather
import pandas as pd

geodesic = pyproj.Geod(ellps='WGS84')


PATH = "../Route Gen/Routes/"
filename = "DFW4"
FILEPATH = PATH + filename + ".csv"

coordinates = np.loadtxt(FILEPATH, delimiter=',')
# print(coordinates)

min_lat, max_lat = coordinates[:, 0].min(), coordinates[:, 0].max()
min_lon, max_lon = coordinates[:, 1].min(), coordinates[:, 1].max()

# print((min_lat, min_lon), (max_lat, max_lon))

az12, az21, a = geodesic.inv(
    min_lon, min_lat, max_lon, max_lat)

new_min = geopy.distance.distance(meters=100).destination(
    point=(min_lat, min_lon), bearing=(az12-180) % 360)

new_min = (new_min.latitude, new_min.longitude)

new_max = geopy.distance.distance(meters=100).destination(
    point=(max_lat, max_lon), bearing=(az21-180) % 360)
new_max = (new_max.latitude, new_max.longitude)

lat_dist = geopy.distance.distance(
    (new_min[0], new_min[1]), (new_max[0], new_min[1])).meters

lon_dist = geopy.distance.distance(
    (new_min[0], new_min[1]), (new_min[0], new_max[1])).meters

lat_interval = int(np.ceil(lat_dist / 150))
lon_interval = int(np.ceil(lon_dist / 150))

print(lat_interval, lon_interval)

interval_lats = np.linspace(new_min[0], new_max[0], lat_interval)
interval_lons = np.linspace(new_min[1], new_max[1], lon_interval)

# print(interval_lats, interval_lons)

points = np.array(np.meshgrid(interval_lats, interval_lons)).T.reshape(-1, 2)

print(points.shape[0])

alts = [30, 35, 40]
times = ["2021-04-20T05:30:00+00:00",
         "2021-04-20T05:45:00+00:00", "2021-04-20T06:00:00+00:00"]
# alts = [30.0]
# times = ["2021-04-20T05:30:00+00:00"]


qlats, qlons, qalts, qtimes = [], [], [], []

for point in points:
    for alt, time in itertools.product(alts, times):
        qlats.append(float(point[0]))
        qlons.append(float(point[1]))
        qalts.append(float(alt))
        qtimes.append(time)

# plt.scatter(points[:, 1], points[:, 0], c='g')
# plt.plot(coordinates[:, 1], coordinates[:, 0], c='r')
# plt.show()


data = request_weather(
    qlats, qlons, qalts, qtimes)

data = pd.DataFrame([[float(item['lat']), float(item['lon']), float(item['alt']), str(item['timestamp']),
                      float(item['wind_dir']), float(item['wind_speed'])] for _, item in data.items()], columns=['lat', 'lon', 'alt', 'timestamp', 'wind_dir', 'wind_speed'])

print(data.head)
print(data.dtypes)
# print(data.shape)

# np.savetxt(f'out/{filename}-weather.csv', data,
#            fmt="%f,%f,%f,%s,%f,%f", delimiter=',')

data.to_csv(f'out/{filename}-weather.csv', index=False)
