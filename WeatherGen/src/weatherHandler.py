#!/usr/bin/env python
"""Handles collecting the weather information anf formatting it to a python dictionary"""


# Import statements
from typing import List

try:
    from src.wxserver import server_request
except Exception:
    from WeatherGen.src.wxserver import server_request


def request_weather(lats: List[float], lons: List[float], alts: List[float], timestamps: List[str], certificate: str = '', key: str = ''):

    returned = server_request(lats, lons, alts, timestamps, certificate, key)

    data = {}
    for i in range(len(lats)):
        wind_dir, wind_speed = returned[i]['wind_direction'], returned[i]['wind_speed']
        data[i] = {
            'lat': lats[i],
            'lon': lons[i],
            'alt': alts[i],
            'timestamp': timestamps[i],
            'wind_dir': wind_dir,
            'wind_speed': wind_speed
        }

    return data
