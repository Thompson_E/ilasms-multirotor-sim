#!/usr/bin/env python
"""Query the weather server and return the list of weather conditions"""


from time import perf_counter
from typing import List

import requests

CERT = "/home/ellis/wxserver_certificates/wxserver.crt"
KEY = "/home/ellis/wxserver_certificates/wxserver.key"

# CERT = "certificates/wxserver.crt"
# KEY = "certificates/wxserver.key"


def server_request(lats: List[float], lons: List[float], alts: List[float], timestamps: List[str], certificate: str = '', key: str = ''):
    # sourcery skip: raise-specific-error
    """Handles the requestinng of the weather data from the wxserver"""

    # Assert that valid data is passed
    assert len(lats) == len(lons) == len(alts) == len(timestamps)

    no_points = len(lats)

    if not certificate:
        certificate = CERT

    if not key:
        key = KEY

    session = requests.Session()
    session.cert = (certificate, key)

    request_data = {
        'num_elements': no_points,
        'coordinates': {
            'latitudes': lats,
            'longitudes': lons,
            'altitudes_m': alts,
            'timestamps': timestamps,
        }
    }

    print(
        f'[MESSAGE]:: Requesting {no_points} points of data from the weather server')
    start = perf_counter()

    result = session.request(
        'REPORT',
        'https://microwx.wx.ll.mit.edu/get_weather_data',
        json=request_data)

    run_time = perf_counter() - start

    print(
        f'[MESSAGE]:: Response ({result.status_code}) from server in {run_time:.3f} seconds')

    if int(result.status_code) != 200:
        raise Exception(
            f'Invalid server response status_code {result.status_code}')

    return result.json()
