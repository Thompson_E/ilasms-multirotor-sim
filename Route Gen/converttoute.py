import matplotlib.pyplot as plt
import numpy as np
import pyproj
geodesic = pyproj.Geod(ellps='WGS84')

PATH = "Routes/"
filename = "DFW4"
FILEPATH = PATH + filename + ".csv"


coordinates = np.loadtxt(FILEPATH, delimiter=',')
print(coordinates)

converted = np.array([[0, 0]])

for i in range(1, coordinates.shape[0]):
    az12, _, dist = geodesic.inv(
        coordinates[i-1, 1], coordinates[i-1, 0], coordinates[i, 1], coordinates[i, 0])

    x = converted[-1, 0] + dist * np.sin(np.deg2rad(az12))
    y = converted[-1, 1] + dist * np.cos(np.deg2rad(az12))

    x = np.linspace(converted[-1, 0], x, int(np.ceil(dist/10)))
    y = np.linspace(converted[-1, 1], y, int(np.ceil(dist/10)))

    co = np.array(list(zip(x, y)))

    converted = np.append(converted, co, axis=0) if i != 1 else co


index = np.arange(1, converted.shape[0]+1, 1)
x, y = converted[:, 0], converted[:, 1]

x = np.array(list(zip(index, x)), dtype=[('index', int), ('x', float)])
y = np.array(list(zip(index, y)), dtype=[('index', int), ('y', float)])

np.savetxt(f'out/x-{filename}.csv', x, fmt="%i,%f", delimiter=',')
np.savetxt(f'out/y-{filename}.csv', y, fmt="%i,%f", delimiter=',')

plt.plot(converted[:, 0], converted[:, 1])
plt.show()
