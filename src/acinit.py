import numpy as np
from multirotor.controller import (AltController, AttController, Controller,
                                   PosController)
from multirotor.coords import (angular_to_euler_rate, body_to_inertial,
                               direction_cosine_matrix, inertial_to_body)
from multirotor.env import DynamicsMultirotorEnv as LocalOctorotor
from multirotor.helpers import DataLog, control_allocation_matrix
from multirotor.simulation import Battery, Motor, Multirotor, Propeller
from multirotor.trajectories import Trajectory
from multirotor.vehicle import (MotorParams, PropellerParams, SimulationParams,
                                VehicleParams)
from multirotor.visualize import VehicleDrawing


class T18:
    def __init__(self, dt=1e-3, g=9.81, no_propellers=8):

        self.dt = dt
        self.g = g
        self.no_propellers = no_propellers

        self.mp = MotorParams(
            moment_of_inertia=5e-5,
            resistance=0.27,
            k_emf=0.0265
        )

        self.pp = PropellerParams(
            moment_of_inertia=1.86e-6,
            use_thrust_constant=True,
            k_thrust=9.8419e-05,
            k_drag=1.8503e-06,
            motor=self.mp
        )

        self.vp = VehicleParams(
            propellers=[self.pp] * no_propellers,
            angles=np.linspace(0, -2*np.pi, num=no_propellers,
                               endpoint=False) + 0.375 * np.pi,
            distances=np.ones(no_propellers) * 0.635,
            clockwise=[-1, 1, -1, 1, -1, 1, -1, 1],
            mass=10.66,
            inertia_matrix=np.asarray([
                [0.2206, 0, 0],
                [0, 0.2206, 0.],
                [0, 0, 0.4238]
            ])
        )

        self.sp = SimulationParams(dt, g)

        self.initilise()

    def initilise(self):
        self.multirotor = Multirotor(self.vp, self.sp)
        self.multirotor.reset()

        self.env = LocalOctorotor(vehicle=self.multirotor)
        self.env.vehicle.state[2] = 1.

    def initilise_trajectory(self, trajectory, proximity=0.5, resolution=None):
        self.trajectory = Trajectory(
            trajectory, self.env.vehicle, proximity=proximity, resolution=resolution)

        self.ctrl = Controller(
            # (Outer) Position controller has low sensitivity
            PosController(0.5, 0.02, 0., 1., dt=self.dt,
                          vehicle=self.multirotor),
            # (Inner) Attitude controller is more responsive (except for yaw, which we are not controlling)
            AttController(np.asarray([50., 50., 2.]),
                          np.asarray([10., 10., 2.]),
                          np.asarray([0., 0., 0.]), 1., dt=self.dt, vehicle=self.multirotor),
            # Altitude controller is more responsive as well
            AltController(50, 2, 0, 1, dt=self.dt, vehicle=self.multirotor)
        )

        self.log = DataLog(self.env.vehicle, self.ctrl, other_vars=(
            'action', 'target', 'alloc_errs'))

    def step(self, pos, wind=None):

        # Get prescribed dynamics for system
        reference = np.asarray([*pos, 0.])
        dynamics = self.ctrl.step(reference)
        thrust, torques = dynamics[0], dynamics[1:]
        f = np.asarray([0, 0, thrust])
        # Add disturbances

        if wind is not None:
            f[:2] += wind
        # apply to simulation
        state, *_ = self.env.step(np.asarray([*f, *torques]))

        self.log.log(target=pos)

        return state
