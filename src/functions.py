import numpy as np
import pyproj
from math import cos, sin
import pandas as pd

geodesic = pyproj.Geod(ellps='WGS84')


def lat_const(mu0):
    return 111132.92 - 559.82 * cos(2*mu0) + 1.175 * cos(4*mu0) - 0.0023 * cos(6*mu0)


def lon_const(mu0): return 111412.84 * cos(mu0) - \
    93.5 * cos(3*mu0) + 0.118 * cos(5*mu0)


def getXYfromLL(pos, origin, rotation, consts):
    north = pos[0] - origin[0]
    east = pos[1] - origin[1]
    psi = np.deg2rad(-rotation)

    N = north*consts[0]
    E = east*consts[1]

    y = cos(psi) * N - sin(psi) * E
    x = sin(psi) * N + cos(psi) * E

    return [x, y]


def get_translate_consts(_name):
    route_path = _name
    geo_track = np.loadtxt(route_path, delimiter=',')
    geo_ref = geo_track[0]

    offset, _, dist = geodesic.inv(
        geo_track[0, 1], geo_track[0, 0], geo_track[-1, 1], geo_track[-1, 0])

    print(geo_ref)

    _latconst = lat_const(np.deg2rad(geo_ref[0]))
    _lonconst = lon_const(np.deg2rad(geo_ref[0]))

    print(_latconst, _lonconst)

    consts = [_latconst, _lonconst]

    return geo_track, geo_ref, offset, consts


def windtoUV(speed, direction, offset):
    u = speed*np.sin(np.deg2rad(direction-offset))
    v = speed*np.cos(np.deg2rad(direction-offset))
    return u, v


def convert_wind(_name, geo_ref, offset, consts):
    weather = pd.read_csv(_name)
    weather = weather.to_numpy()

    for i in range(weather.shape[0]):
        xy = getXYfromLL(weather[i, 0:2], geo_ref, offset, consts)
        weather[i, 0] = xy[0]
        weather[i, 1] = xy[1]

        u, v = windtoUV(weather[i, 5], weather[i, 4], offset)

        weather[i, 4] = u
        weather[i, 5] = v

    print(weather)
