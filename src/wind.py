import math
from typing import List

import numpy as np

# ------ Defaults ------
DragCo = [0.606, 0.702, 1.192]  # Drag coefficient
Area = [0.062214, 0.0698, 0.167784]  # Area

Temp = 20  # Temperature in Celsius
Pressure = 101.325  # Temperature in kPa


class WindForce:
    def __init__(self, Cd: List[float] = None, A: List[float] = None):

        self.cd = np.array(Cd) if Cd is not None else np.array(DragCo)
        self.A = np.array(A) if A is not None else np.array(Area)

        self.kB = 1.38064852e-23
        self.mD = 4.81e-26

    def get_drag(self, v: List[float], wv: List[float], rho: float = 1.225):
        v, wv = np.array(v), np.array(wv)

        a = self.cd*self.A

        relv = (v - wv) ** 2

        return 0.5*(rho*relv*a)

    def get_rhoT(self, T, P):
        "Get rho based on Temperature in C and pressure in kPa"

        RSpecific = self.kB/self.mD

        T = T + 273.15  # Convert to Kelvin
        P = P*1000  # Convert to Pa
        top = P*self.mD
        bottom = self.kB*T

        return top/bottom
