import numpy as np


def interpolate(point, data):
    lower, upper = get_bounds(point[-1], data[:, -1])

    print(upper, lower)

    data = data[data[:, -1] <= upper]
    data = data[data[:, -1] >= lower]


def get_bounds(a, data):
    data = sorted(data)

    if data[-1] <= a:
        return data[-1], data[-1]

    if data[0] >= a:
        return data[0], data[0]

    i = 0
    j = len(data) - 1

    while data[i] < a and i < len(data)-1:
        i += 1

    if data[i] > a:
        i -= 1

    while data[j] > a and j > 0 and j >= i:
        j -= 1

    if data[j] < a:
        j += 1

    return data[i], data[j]


data = np.array([[1, 10], [2, 10], [3, 10], [1, 20], [2, 20],
                [3, 20], [1, 30], [2, 30], [3, 30]])

a = [2.6, 22]

interpolate(a, data)
